const conf = require('config');

if (conf) {
    var redis = require('./redis.js');
    var mysql = require('./mysql.js');
}

// Exports the different query types
module.exports = {

    // To get data from DB/Redis
    // what = query
    // limit = number of results wanted
    get(res, what, limit = null) {
        redis.get(what)
        .then( (redisAnswer) => {
            // console.log('Pasó redis: ', redisAnswer);
            
            if (redisAnswer === null || redisAnswer === undefined) {
                                
                mysql.get(what, limit)
                .then( (mysqlAnswer) => {
                    if (limit === 1) {
                        mysqlAnswer = mysql.singularify(mysqlAnswer);
                    }
                    redis.client.set(what, JSON.stringify(mysqlAnswer), redis.client.print);
                    res.send(mysqlAnswer)

                })
                .catch( (error) => {
                    console.log(error);
                })

            } else {
                res.send(redisAnswer)
            }

        })
        .catch( (error) => {
            console.log(error);
        })

    }
};
