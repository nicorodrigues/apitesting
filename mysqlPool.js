const mysql = require('mysql');
const conf = require('config');

// Mysql connection generator
var client = mysql.createPool({
    "connectionLimit": 100,
    "host": conf.mysql.host,
    "user": conf.mysql.user,
    "password": conf.mysql.password,
    "database": conf.mysql.database
});


// Recover data from DB
function get(what, limit) {
    let actualLimit = limit === null ? '' : ' LIMIT ' + limit;
    
    // We return a promise so we can make the system syncrhonous
    return new Promise((resolve,  reject)  => {
        
        client.getConnection(function(err, connection) {
            if (err) {
                connection.release();
                console.log(' Error getting mysql_pool connection: ' + err);
                throw err;
            }
            client.query(what + actualLimit, function (err, results) {
                if (err) throw err
                
                response = {};
                for (let i = 0; i < results.length; i++) {
                    response[i] = results[i];
                }
                
                console.log('MYSQL:', response);
                
                connection.release();
                resolve(response);
            });
        });
    });
    
}

// Transforms the first child of a JSON into the entire JSON
function singularify(json) {
    
    return json[Object.keys(json)[0]];
    
}


// Exports the client itself and the different functions so they can be easily accessed
module.exports = {
    client: client,
    get: get,
    singularify: singularify
}
