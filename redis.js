const redis = require("redis");
const conf = require('config');
var workerId = require('./clusterize.js').clusterId;    // This identifies the actual worker of the cluster

if (workerId === undefined) workerId = 'Master'; // If it's not a worker, we name it Master

var client = redis.createClient({

    "host": conf.redis.host,
    retry_strategy: function (options) {
        if (options.error && options.error.code === 'ECONNREFUSED' && options.attempt > 1000) {
            // End reconnecting on a specific error and flush all commands with
            // a individual error
            return new Error('The server refused the connection');
        }
        if (options.total_retry_time > 1000 * 60 * 60) {
            // End reconnecting after a specific timeout and flush all commands
            // with a individual error
            return new Error('Retry time exhausted');
        }
        if (options.attempt > 100) {
            // End reconnecting with built in error
            return new Error('Max attempts');
        }
        // reconnect after
        console.log('Worker ID: ', workerId);
        console.log(`Volviendo a reconectar, intento: ${options.attempt}`);
        return Math.min(options.attempt * 1000, 15000);
    }

});

client.on('ready', () => {

    console.log(`Worker ${workerId}: conectado.`);

});

const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);


async function get(what) {

    if (client.connected) {
        datos = await getAsync(what);
        if (datos !== null) console.log('REDIS:', JSON.parse(datos));

        return datos === null ? null : JSON.parse(datos);
    }
    
}

module.exports = {
    client: client,
    get: get
}
