const express = require('express');
const app = express();
var query = require('./query.js');
var cluster = require('./clusterize.js');


// Check if this is a worker
if (!cluster.cluster.isMaster) {
    app.get('/', (req, res) => {
        query.get(res, 'SELECT * FROM manuals', 2);
    });

    app.get('/test', (req, res) => {
        query.get(res, 'SELECT * FROM manuals WHERE id = 1', 1);
    });

    app.get('/down', (req, res) => {
        res.send(2/0);
    });

    app.get('/html', (req, res) => {
        query.get(res, 'SELECT * FROM manuals WHERE id = 4', 1);
    })

    app.get('/home', (req, res) => {
        query.get(res, 'SELECT * FROM manuals WHERE id = 5', 1);
    })


    app.listen(3000, () => console.log(`Worker ID: ${cluster.clusterId} - Example app listening on port 3000!`));
}
