var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

if (cluster.isMaster) {

    // Fork workers.
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    // error message on dead worker
    cluster.on('exit', function(worker, code, signal) {
        console.log('worker ' + worker.process.pid + ' died');
    });
} else {
    // worker id export so we can identify error's owner on other modules
    module.exports.clusterId = cluster.worker.id;
}

// exporting the cluster
module.exports.cluster = cluster;
