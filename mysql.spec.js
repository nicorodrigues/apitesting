'use strict'

const Mysql = require('./mysql');
const expect = require('chai').expect;

describe('Mysql module', () => {
    describe('"client"', () => {
        it('should export a function', () => {
            expect(Mysql.client).to.be.a('object');
        });
    });
    
    describe('"singularify"', () => {
        it('should export a function', () => {
            expect(Mysql.singularify).to.be.a('function');
        });
    });
    
    describe('"get"', () => {
        it('should export a function', () => {
            expect(Mysql.get).to.be.a('function');
        })
        it('should return a Promise', async () => {
            const limit = 1;
            const mysqlGetResult = Mysql.get('SELECT * FROM manuals', limit);
            expect(mysqlGetResult).to.be.a('Promise');
            expect(mysqlGetResult.then).to.be.a('Function');
            
            await mysqlGetResult.then((elem) => {
                describe(`"get responses: ${Object.keys(elem).length}"`, function () {
                    
                    it('should be an object', function () {
                        expect(elem).to.be.a('Object');
                    });
                    if (limit === 1) {
                        elem = Mysql.singularify(elem);
                        it('should have id and name', function () {
                            expect(elem).to.have.property('id');
                            expect(elem).to.have.property('name');
                        });
                    } else {
                        for (var key in elem) {
                            if (elem.hasOwnProperty(key)) {
                                var obj = elem[key];
                                it(`${Number(key) + 1}: should have id and name`, function () {
                                    expect(obj).to.have.property('id');
                                    expect(obj).to.have.property('name');
                                });
                            }
                        }
                        
                    }
                });
                expect(mysqlGetResult.catch).to.be.a('Function');
            }); 
        });
    });
});